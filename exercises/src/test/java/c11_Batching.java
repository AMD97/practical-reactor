import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

/**
 * Another way of controlling amount of data flowing is batching.
 * Reactor provides three batching strategies: grouping, windowing, and buffering.
 *
 * Read first:
 *
 * https://projectreactor.io/docs/core/release/reference/#advanced-three-sorts-batching
 * https://projectreactor.io/docs/core/release/reference/#which.window
 *
 * Useful documentation:
 *
 * https://projectreactor.io/docs/core/release/reference/#which-operator
 * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Mono.html
 * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Flux.html
 *
 * @author Stefan Dragisic
 */
class c11_Batching extends BatchingBase {

    /**
     * To optimize disk writing, write data in batches of max 10 items, per batch.
     */
    @Test
    void batch_writer() {

        Flux<Void> dataStream = dataStream()
                .buffer(10)
                .flatMap(this::writeToDisk);

        //do not change the code below
        StepVerifier.create(dataStream)
                .verifyComplete();

        Assertions.assertEquals(10, diskCounter.get());
    }

    /**
     * You are implementing a command gateway in CQRS based system. Each command belongs to an aggregate and has `aggregateId`.
     * All commands that belong to the same aggregate needs to be sent sequentially, after previous command was sent, to
     * prevent aggregate concurrency issue.
     * But commands that belong to different aggregates can and should be sent in parallel.
     * Implement this behaviour by using `GroupedFlux`, and knowledge gained from the previous exercises.
     */
    @Test
    void command_gateway() {

        Flux<Void> processCommands = inputCommandStream()
                .groupBy(Command::getAggregateId)
                .flatMap(groupFlux -> groupFlux
                        .concatMap(this::sendCommand));

        //do not change the code below
        Duration duration = StepVerifier.create(processCommands)
                .verifyComplete();

        Assertions.assertTrue(duration.getSeconds() <= 3, "Expected to complete in less than 3 seconds");
    }


    /**
     * You are implementing time-series database. You need to implement `sum over time` operator. Calculate sum of all
     * metric readings that have been published during one second.
     */
    @Test
    void sum_over_time() {

        Flux<Long> metrics = metrics()
                .buffer(Duration.ofSeconds(1))
                .map(longs -> longs.stream().reduce(Long::sum).orElse(0L))
                .take(10);

        StepVerifier.create(metrics)
                .expectNext(45L, 165L, 255L, 396L, 465L, 627L, 675L, 858L, 885L, 1089L)
                .verifyComplete();
    }

    @Test
    void exploreBuffer() throws InterruptedException {

        var latch = new CountDownLatch(1);

        Flux.range(1, 100)
                .delayElements(Duration.ofMillis(10))
                .bufferTimeout(2, Duration.ofMillis(30))
                .subscribe(System.out::println, System.err::println, latch::countDown);

        Assertions.assertTrue(latch.await(5, TimeUnit.SECONDS));
    }

    @Test
    void exploreWindow() throws InterruptedException {

        var latch = new CountDownLatch(1);

        Flux.range(1, 100)
                .delayElements(Duration.ofMillis(10))
                .window(2)
                .concatMap(integerFlux -> integerFlux)
                .subscribe(System.out::println, System.err::println, latch::countDown);

        Assertions.assertTrue(latch.await(5, TimeUnit.SECONDS));
    }

    @Test
    void exploreGroupBy() throws InterruptedException {

        var latch = new CountDownLatch(1);

        Flux.range(1, 100)
                .delayElements(Duration.ofMillis(50))
                .groupBy(i -> i % 2)
                .subscribe(
                        group -> group.subscribe(val -> System.out.printf("val: %s, group key: %s\n", val, group.key())),
                        System.err::println,
                        latch::countDown
                );

        Assertions.assertTrue(latch.await(5, TimeUnit.SECONDS));
    }

}
