import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.test.StepVerifier;

import static java.lang.System.out;
import static java.util.concurrent.CompletableFuture.runAsync;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * In Reactor a Sink allows safe manual triggering of signals. We will learn more about multicasting and backpressure in
 * the next chapters.
 *
 * Read first:
 *
 * https://projectreactor.io/docs/core/release/reference/#sinks
 * https://projectreactor.io/docs/core/release/reference/#processor-overview
 *
 * Useful documentation:
 *
 * https://projectreactor.io/docs/core/release/reference/#which-operator
 * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Mono.html
 * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Flux.html
 *
 * @author Stefan Dragisic
 */
class c8_Sinks extends SinksBase {

    /**
     * You need to execute operation that is submitted to legacy system which does not support Reactive API. You want to
     * avoid blocking and let subscribers subscribe to `operationCompleted` Mono, that will emit `true` once submitted
     * operation is executed by legacy system.
     */
    @Test
    void single_shooter() {

        Sinks.One<Boolean> sink = Sinks.one();
        Mono<Boolean> operationCompleted = sink.asMono();
        submitOperation(() -> {
            doSomeWork();
            sink.tryEmitValue(true);
        });


        //don't change code below
        StepVerifier.create(operationCompleted.timeout(Duration.ofMillis(5500))).expectNext(true).verifyComplete();
    }

    /**
     * Similar to previous exercise, you need to execute operation that is submitted to legacy system which does not
     * support Reactive API. This time you need to obtain result of `get_measures_reading()` and emit it to subscriber.
     * If measurements arrive before subscribers subscribe to `get_measures_readings()`, buffer them and emit them to
     * subscribers once they are subscribed.
     */
    @Test
    void single_subscriber() {

        Sinks.Many<Integer> sink = Sinks.many().unicast().onBackpressureBuffer();
        Flux<Integer> measurements = sink.asFlux();
        submitOperation(() -> {
            List<Integer> measures_readings = get_measures_readings(); //don't change this line
            measures_readings.forEach(sink::tryEmitNext);
            sink.tryEmitComplete();
        });

        //don't change code below
        StepVerifier.create(measurements.delaySubscription(Duration.ofSeconds(6))).expectNext(0x0800, 0x0B64, 0x0504)
                .verifyComplete();
    }

    /**
     * Same as previous exercise, but with twist that you need to emit measurements to multiple subscribers.
     * Subscribers should receive only the signals pushed through the sink after they have subscribed.
     */
    @Test
    void it_gets_crowded() {

        Sinks.Many<Integer> sink = Sinks.many().multicast().onBackpressureBuffer();
        Flux<Integer> measurements = sink.asFlux();
        submitOperation(() -> {
            List<Integer> measures_readings = get_measures_readings(); //don't change this line
            measures_readings.forEach(sink::tryEmitNext);
            sink.tryEmitComplete();
        });

        //don't change code below
        StepVerifier.create(Flux.merge(measurements.delaySubscription(Duration.ofSeconds(1)), measurements.ignoreElements()))
                .expectNext(0x0800, 0x0B64, 0x0504).verifyComplete();
    }

    /**
     * By default, if all subscribers have cancelled (which basically means they have all un-subscribed), sink clears
     * its internal buffer and stops accepting new subscribers. For this exercise, you need to make sure that if all
     * subscribers have cancelled, the sink will still accept new subscribers. Change this behavior by setting the
     * `autoCancel` parameter.
     */
    @Test
    void open_24_7() {

        Sinks.Many<Integer> sink = Sinks.many().multicast().onBackpressureBuffer(Integer.MAX_VALUE, false);
        Flux<Integer> flux = sink.asFlux();

        //don't change code below
        submitOperation(() -> {
            get_measures_readings().forEach(sink::tryEmitNext);
            submitOperation(sink::tryEmitComplete);
        });

        //subscriber1 subscribes, takes one element and cancels
        StepVerifier sub1 = StepVerifier.create(Flux.merge(flux.take(1)))
                                        .expectNext(0x0800)
                                        .expectComplete()
                                        .verifyLater();

        //subscriber2 subscribes, takes one element and cancels
        StepVerifier sub2 = StepVerifier.create(Flux.merge(flux.take(1)))
                                        .expectNext(0x0800)
                                        .expectComplete()
                                        .verifyLater();

        //subscriber3 subscribes after all previous subscribers have cancelled
        StepVerifier sub3 = StepVerifier.create(flux.take(3)
                                                    .delaySubscription(Duration.ofSeconds(6)))
                                        .expectNext(0x0B64) //first measurement `0x0800` was already consumed by previous subscribers
                                        .expectNext(0x0504)
                                        .expectComplete()
                                        .verifyLater();

        sub1.verify();
        sub2.verify();
        sub3.verify();
    }

    /**
     * If you look closely, in previous exercises third subscriber was able to receive only two out of three
     * measurements. That's because used sink didn't remember history to re-emit all elements to new subscriber.
     * Modify solution from `open_24_7` so third subscriber will receive all measurements.
     */
    @Test
    void blue_jeans() {

        Sinks.Many<Integer> sink = Sinks.many().replay().all();
        Flux<Integer> flux = sink.asFlux();

        //don't change code below
        submitOperation(() -> {
            get_measures_readings().forEach(sink::tryEmitNext);
            submitOperation(sink::tryEmitComplete);
        });

        //subscriber1 subscribes, takes one element and cancels
        StepVerifier sub1 = StepVerifier.create(Flux.merge(flux.take(1)))
                                        .expectNext(0x0800)
                                        .expectComplete()
                                        .verifyLater();

        //subscriber2 subscribes, takes one element and cancels
        StepVerifier sub2 = StepVerifier.create(Flux.merge(flux.take(1)))
                                        .expectNext(0x0800)
                                        .expectComplete()
                                        .verifyLater();

        //subscriber3 subscribes after all previous subscribers have cancelled
        StepVerifier sub3 = StepVerifier.create(flux.take(3)
                                                    .delaySubscription(Duration.ofSeconds(6)))
                                        .expectNext(0x0800)
                                        .expectNext(0x0B64)
                                        .expectNext(0x0504)
                                        .expectComplete()
                                        .verifyLater();

        sub1.verify();
        sub2.verify();
        sub3.verify();
    }


    /**
     * There is a bug in the code below. May multiple producer threads concurrently generate data on the sink?
     * If yes, how? Find out and fix it.
     */
    @Test
    void emit_failure() {

        Sinks.Many<Integer> sink = Sinks.many().replay().all();

        for (int i = 1; i <= 50; i++) {
            int finalI = i;
            new Thread(() -> sink.emitNext(
                    finalI,
                    (signal, result) -> result.equals(Sinks.EmitResult.FAIL_NON_SERIALIZED))
            ).start();
        }

        //don't change code below
        StepVerifier.create(sink.asFlux().doOnNext(out::println).take(50)).expectNextCount(50).verifyComplete();
    }

    @Test
    void exploreSinkOne() throws InterruptedException {
        var latch = new CountDownLatch(1);

        Sinks.One<String> sinkOne = Sinks.one();
        Mono<String> sinkMono = sinkOne.asMono();

        out.println("subscribe");
        sinkMono.subscribe(out::println, System.err::println, latch::countDown);

        out.println("sleep 1 seconds...");
        SECONDS.sleep(1);

        out.println("emmit");
        sinkOne.tryEmitValue("amd");

        latch.await(10, SECONDS);
    }

    @Test
    void exploreSinkManyUnicast() throws InterruptedException {
        var latch = new CountDownLatch(1);

        Sinks.Many<String> sinkUnicast = Sinks.many().unicast().onBackpressureBuffer();
        Flux<String> sinkFlux = sinkUnicast.asFlux();

        out.println("sleep 1 seconds...");
        SECONDS.sleep(1);

        out.println("emmit amd");
        sinkUnicast.tryEmitNext("amd");

        out.println("sleep 1 seconds...");
        SECONDS.sleep(1);

        out.println("emmit pedram");
        sinkUnicast.tryEmitNext("pedram");

        out.println("emmit competition");
        sinkUnicast.tryEmitComplete();

        out.println("start sub 1");
        sinkFlux.subscribe(val -> out.printf("sub 1 : %s\n", val), err -> System.err.printf("sub 1 : %s\n", err), latch::countDown);

        Assertions.assertTrue(latch.await(10, SECONDS));

    }

    @Test
    void exploreSinkManyMulticast() throws InterruptedException {
        var latch = new CountDownLatch(2);

        Sinks.Many<String> sinkMulticast = Sinks.many().multicast().onBackpressureBuffer();
        Flux<String> sinkFlux = sinkMulticast.asFlux();

        out.println("sleep 1 seconds...");
        SECONDS.sleep(1);

        out.println("emmit amd");
        sinkMulticast.tryEmitNext("amd");

        out.println("sleep 1 seconds...");
        SECONDS.sleep(1);

        out.println("emmit pedram");
        sinkMulticast.tryEmitNext("pedram");

        out.println("start sub 1");
        sinkFlux.subscribe(val -> out.printf("sub 1 : %s\n", val), err -> System.err.printf("sub 1 : %s\n", err), latch::countDown);

        out.println("start sub 2");
        sinkFlux.subscribe(val -> out.printf("sub 2 : %s\n", val), err -> System.err.printf("sub 2 : %s\n", err), latch::countDown);

        out.println("sleep 1 seconds...");
        SECONDS.sleep(1);

        out.println("emmit reza");
        sinkMulticast.tryEmitNext("reza");

        out.println("emmit competition");
        sinkMulticast.tryEmitComplete();

        Assertions.assertTrue(latch.await(10, SECONDS));
    }

    @Test
    void exploreSinkManyTreadSafety() throws InterruptedException {
        var latch = new CountDownLatch(1);

        Sinks.Many<Integer> sinkUnicast = Sinks.many().unicast().onBackpressureBuffer();
        Flux<Integer> sinkFlux = sinkUnicast.asFlux();

        List<Integer> subList = new ArrayList<>();
        Map<Integer, Integer> failedEmmit = new ConcurrentHashMap<>();
        AtomicInteger failedEmmitCounter = new AtomicInteger();

        out.println("start sub 1");
        sinkFlux.subscribe(subList::add, err -> System.err.printf("sub 1 : %s\n", err), latch::countDown);

        out.println("emmit 1000 numbers");
        for (int i = 1; i <= 1000; i++) {

            if (i == 1000) {
                out.println("emmit completion");
                SECONDS.sleep(2);
                sinkUnicast.emitComplete((signalType, emitResult1) -> true);
            } else {
                final int emmitValue = i;
                runAsync(() -> {
                    sinkUnicast.emitNext(emmitValue, (signalType, emitResult1) -> {
                        if (emitResult1.isFailure()) {
                            failedEmmitCounter.incrementAndGet();
                            failedEmmit.compute(emmitValue, (key, value) -> value == null ? 1 : ++value);
                        }
                        return true;
                    });
                });
            }

        }

        Assertions.assertTrue(latch.await(3, SECONDS));

        out.printf("sub received: %s\n", subList.size());
        out.printf("failed emmit counter: %s\n", failedEmmitCounter.get());
        out.printf("failed emmit: %d(counter)    %d(number)\n", failedEmmit.size(), failedEmmit.values().stream()
                .reduce(Integer::sum).get());
    }

    @Test
    void exploreSinkManyMulticastDirectAllOrNothing() throws InterruptedException {
        var latch = new CountDownLatch(2);

        Sinks.Many<String> sinkMulticast = Sinks.many().multicast().directAllOrNothing();
        Flux<String> sinkFlux = sinkMulticast.asFlux();

        out.println("sleep 1 seconds...");
        SECONDS.sleep(1);

        out.println("emmit amd");
        sinkMulticast.tryEmitNext("amd");

        out.println("sleep 1 seconds...");
        SECONDS.sleep(1);

        out.println("emmit pedram");
        sinkMulticast.tryEmitNext("pedram");

        out.println("start sub 1");
        sinkFlux.subscribe(val -> out.printf("sub 1 : %s\n", val), err -> System.err.printf("sub 1 : %s\n", err), latch::countDown);

        out.println("start sub 2");
        sinkFlux.subscribe(val -> out.printf("sub 2 : %s\n", val), err -> System.err.printf("sub 2 : %s\n", err), latch::countDown);

        out.println("sleep 1 seconds...");
        SECONDS.sleep(1);

        out.println("emmit reza");
        sinkMulticast.tryEmitNext("reza");

        out.println("emmit competition");
        sinkMulticast.tryEmitComplete();

        Assertions.assertTrue(latch.await(10, SECONDS));
    }
}
